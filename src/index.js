import React, { Component } from 'react';
import { render } from 'react-dom';
import SVGGraph from './components/svgGraph';
import PoseNet from './components/posenet';
import peakDetection from './peakDetection';
import './style.css';

const minScore = 0.15;
const smoothAlpha = .1;

class App extends Component {
  constructor() {
    super();
    this.nodes = [
      { id: 'nose', r: 25 },
      { id: 'leftShoulder', r: 5 },
      { id: 'rightShoulder', r: 5 },
      { id: 'leftElbow', r: 5 },
      { id: 'rightElbow', r: 5 },
      { id: 'leftWrist', r: 10 },
      { id: 'rightWrist', r: 10 },
      { id: 'leftHip', r: 5 },
      { id: 'rightHip', r: 5 },
      { id: 'leftKnee', r: 5 },
      { id: 'rightKnee', r: 5 },
      { id: 'leftAnkle', r: 10 },
      { id: 'rightAnkle', r: 10 },
    ];
    this.links = [
      { source: 'leftShoulder', target: 'rightShoulder' },
      { source: 'rightWrist', target: 'rightElbow' },
      { source: 'rightElbow', target: 'rightShoulder' },
      { source: 'leftWrist', target: 'leftElbow' },
      { source: 'leftElbow', target: 'leftShoulder' },
      { source: 'rightAnkle', target: 'rightKnee' },
      { source: 'rightKnee', target: 'rightHip' },
      { source: 'rightHip', target: 'rightShoulder' },
      { source: 'leftAnkle', target: 'leftKnee' },
      { source: 'leftKnee', target: 'leftHip' },
      { source: 'leftHip', target: 'leftShoulder' },
      { source: 'leftHip', target: 'rightHip' },
    ]

    this.wristPos = [];
    this.wristConfidence = [];
    this.extremaCount = 0;
  }

  getNodesWithPosition(poses){
    return this.nodes.reduce((arr, node) => {
      const pose = poses[node.id] || {};
      const position = (pose.position || {});

      if(pose.score < minScore) {
        return arr;
      }

      return [...arr, { ...node, ...position }];
    }, []);
  }

  getLinks(nodes){
    const nodesIDs = nodes.map(node => node.id);

    return this.links.filter(link => {
      const source = (link.source || {}).id || link.source;
      const target = (link.target || {}).id || link.target;

      return nodesIDs.includes(source) && nodesIDs.includes(target);
    });
  }

  getWristPosition(poses){

  // const nodes = this.getNodesWithPosition(poses);

   if (poses.rightWrist.score > minScore) {

    //  const paired  = this.pair(poses.rightWrist.position.x, poses.rightWrist.position.y);
     this.wristPos.push({ x: new Date(), y: poses.rightWrist.position.y });
     this.calculateExtrema()
   }

    return this.wristPos;
  }

  getWristConfidence(poses){

   // const nodes = this.getNodesWithPosition(poses);

   if (poses.rightWrist.score > minScore) {

     this.wristConfidence.push({ x: new Date(), y: poses.rightWrist.score });
   }

    return this.wristConfidence;
  }

  pair(x, y) {

    return (x >= y) ? (x * x + x + y)^3 : (y * y + x)^3;
  }

  average (v) {
    return v.reduce((a,b) => a+b, 0)/v.length;
  }

  smooth(vector, variance) {
    var t_avg = this.average(vector)*variance;
    var ret = Array(vector.length);
    for (var i = 0; i < vector.length; i++) {
        (() => {
        var prev = i>0 ? ret[i-1] : vector[i];
        var next = i<vector.length ? vector[i] : vector[i-1];
        ret[i] = this.average([t_avg, this.average([prev, vector[i], next])]);
      })();
    }
    return ret;
  }

  //calculateExtrema attempts to count reps via looking at
  // local minima/maxima. TODO: figure out better smoothing algo
  // currently, waaay too sensitive to changes
  calculateExtrema() {

      const arrToSmooth = Object.assign([], this.wristPos.map(point => point.y))

      const a = peakDetection(arrToSmooth);
      if (a.length < 10) return
      const n = a.length;

      let count = 0;

      let i;
      for (i=0;i<n-1; i++) {
        count += (a[i] == 1 && a[i+1] == 1 && a[i-1] == 0) ? 1 : 0;
      }

      console.log('Have count: ', count)
      this.extremaCount = count
  }

  render() {
    const videoSize = 300;

    return (
      <PoseNet videoSize={videoSize}>
      {
        ({ poses, loading }) => (
          loading
          ? 'Loading...'
          : (
              <div>
                <SVGGraph
                  graph={this.getWristPosition(poses)}
                />
                <SVGGraph
                  graph={this.getWristConfidence(poses)}
                />
                <div className="reps">reps: {this.extremaCount}</div>
              </div>
          ))
      }
      </PoseNet>

    );
  }
}

render(<App />, document.getElementById('root'));
