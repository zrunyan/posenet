import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { render } from 'react-dom';
import * as d3 from 'd3';

class SVGGraph extends Component {

    constructor(props) {
        super(props);

        this.margin = 20;
        this.line = () => null;
    }

  componentDidMount() {
    this.draw();
    window.addEventListener('resize', this.redraw);
  }

  componentWillReceiveProps() {
    this.redraw();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.redraw);
  }

  containerSize() {
    return {
      width: this.container.clientWidth,
      height: this.container.clientHeight,
    }
  }

  redraw = () => {
    if (this.svg) {
      this.svg.selectAll('*').remove();
    }

    this.draw();
  }

    draw(){


        const { width, height } = this.containerSize()
        this.width = width;
        this.height = height;

        this.h = height - 2 * this.margin
        this.w = width - 2 * this.margin

        //number formatter
        const xFormat = d3.format('.2')

        this.data = this.props.graph;

        //x scale
        const x = d3.scaleLinear()
          .domain(d3.extent(this.data, d => d.x)) //domain: [min,max] of a
          .range([this.margin, this.w])

        //y scale
        const y = d3.scaleLinear()
          .domain([0, d3.max(this.data, d => d.y)]) // domain [0,max] of b (start from 0)
          .range([this.h, this.margin])

        //line generator: each point is [x(d.a), y(d.b)] where d is a row in data
        // and x, y are scales (e.g. x(10) returns pixel value of 10 scaled by x)
        this.line = d3.line()
          .x(d => x(d.x))
          .y(d => y(d.y))
          .curve(d3.curveCatmullRom.alpha(0.9)) //curve line

        this.xTicks = x.ticks(6).map(d => (
            x(d) > this.margin && x(d) < this.w ?
              <g transform={`translate(${x(d)},${this.h + this.margin})`}>
                <text>{xFormat(d)}</text>
                <line x1='0' x1='0' y1='0' y2='5' transform="translate(0,-20)"/>
              </g>
            : null
        ))

        this.yTicks = y.ticks(5).map(d => (
            y(d) > 10 && y(d) < this.h ?
              <g transform={`translate(${this.margin},${y(d)})`}>
                <text x="-12" y="5">{xFormat(d)}</text>
                <line x1='0' x1='5' y1='0' y2='0' transform="translate(-5,0)"/>
                <line className='gridline' x1='0' x1={this.w - this.margin} y1='0' y2='0' transform="translate(-5,0)"/>
              </g>
            : null
        ))

    }
    render() {

        return  (
          <div
            className="container"
            ref={ref => { this.container = ref; }}
          >
            <svg ref={ref => { this.mySvg = ref;  }} width={this.width} height={this.height}>
               <line className="axis" x1={this.margin} x2={this.w} y1={this.h} y2={this.h}/>
               <line className="axis" x1={this.margin} x2={this.margin} y1={this.margin} y2={this.h}/>
               <path d={this.line(this.data)}/>
               <g className="axis-labels">
                 {this.xTicks}
               </g>
               <g className="axis-labels">
                 {this.yTicks}
               </g>
            </svg>
          </div>
      )
  }
}

export default SVGGraph;
