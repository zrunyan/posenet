const lag = 5;
const threshold = 3.5;
const influence = 0.5;

const samples = [
  1.0, 1.0, 1.1, 1.0, 0.9, 1.0, 1.0, 1.1, 1.0, 0.9, 1.0, 1.1, 1.0, 1.0, 0.9, 1.0,
  1.0, 1.1, 1.0, 1.0, 1.0, 1.0, 1.1, 0.9, 1.0, 1.1, 1.0, 1.0, 0.9, 1.0, 1.1, 1.0, 1.0, 1.1, 1.0, 0.8,
  0.9, 1.0, 1.2, 0.9, 1.0, 1.0, 1.1, 1.2, 1.0, 1.5, 1.0, 3.0, 2.0, 5.0, 3.0, 2.0, 1.0, 1.0, 1.0,
  0.9, 1.0, 1.0, 3.0, 2.6, 4.0, 3.0, 3.2, 2.0, 1.0, 1.0, 0.8, 4.0, 4.0, 2.0, 2.5, 1.0, 1.0, 1.0
];

const zScore = (samples) => {
  let stats = [],
    signals = [],
    filteredY = [],
    avgFilter = [],
    stdFilter = [];

  samples.slice(0, lag).forEach(x => stats.push(x))
  avgFilter[lag - 1] = calcMean(stats)
  stdFilter[lag - 1] = stdDev(stats)
  stats = [];

  for (let i = lag; i < samples.length - 1; i++) {
    if (Math.abs(samples[i] - avgFilter[i - 1]) > threshold * stdFilter[i - 1]) {
      signals[i] = (samples[i] > avgFilter[i - 1]) ? 1 : -1
      filteredY[i] = (influence * samples[i]) + ((1 - influence) * filteredY[i - 1])
    } else {
      signals[i] = 0
      filteredY[i] = samples[i]
    }
    for (let j = i - lag; j < i - 1; j++) stats.push(filteredY[j])
    avgFilter[i] = calcMean(stats)
    stdFilter[i] = stdDev(stats)
    stats = [];
  }

  return { signals, avgFilter, stdFilter }
}

const init = (input) => {

  const thresholdingResults = zScore(input);
  const { signals, avgFilter, stdFilter } = thresholdingResults;


  console.log("Signals: ", signals);
// console.log("avgFilter: ", avgFilter);
// console.log("stdFilter: ", stdFilter);
    return signals

}

const calcSum = arr => arr.reduce((acc, cur) => acc + cur)
const calcMean = arr => calcSum(arr) / arr.length

const stdDev = arr => {
  const mean = calcMean(arr);
  const sum = calcSum(arr.map(d => Math.pow(d - mean, 2)));
  return Math.sqrt((sum) / (arr.length - 1))
}

export default init


/*
  [1, 2, 3, 4]
  2.5

  [1, 2, 3, 4, 4] = 2.8
  [2.5, 2.5, 2.5, 2.5, 4]
  ((2.5 * 4) + 4) / 5
*/

